import json
import socket
import sys
import math
import random

class Car(object):
	def __init__(self, car):
		pass

class Piece(object):

	"""
		index
		track
		start
		length
		switch
		radius
		angle
		vmax
	"""

	def __init__(self, piece, index, delta, start):
		self.index  = index
		self.start  = start
		self.length = piece.get('length')
		self.switch = piece.get('switch') == True
		self.radius = radius = float(piece.get('radius',0))
		self.angle  = float(piece.get('angle' ,0))

		self.next_turn          = -1
		self.distance_to_turn   = -1
		self.next_switch        = -1
		self.distance_to_switch = -1
		self.turbo              = False

		if self.angle:
			if self.angle > 0:
				self.radius -= delta
			else:
				self.radius += delta
			self.length = 0.01745329251 * abs(self.angle) * self.radius;
			self.vmax = math.sqrt(0.4*self.radius)
			#usa = 9.0
			#germany = 4.5
			#finland 6.4

	def update_vmax(self,m):
		self.vmax = math.sqrt(m*self.radius)

class Lane(object):

	"""
		id
		delta
		pieces
	"""

	def __init__(self, lane):
		self.id    = int(lane['index'])
		self.delta = int(lane['distanceFromCenter'])

	def set_track_pieces(self, track_pieces):
		start = 0
		pieces = []
		for index,piece in enumerate(track_pieces):
			p = Piece(piece, index, self.delta, start)
			start += p.length
			pieces.append(p)
		self.pieces = pieces
		self.total_pieces = len(pieces)

		i = 0;
		distance_to_turn = 0
		next_turn        = 0
		curr_piece = self.get_piece(i)
		prev_piece = self.get_prev_piece(i)
		while True:
			if curr_piece.angle != 0:
				distance_to_turn = 0
				next_turn = curr_piece.index
			else:
				distance_to_turn += curr_piece.length

			if (prev_piece.distance_to_turn,prev_piece.next_turn) == (distance_to_turn,next_turn):
				break
			else:
				prev_piece.distance_to_turn = distance_to_turn
				prev_piece.next_turn        = next_turn
				i -= 1
				curr_piece = prev_piece
				prev_piece = self.get_prev_piece(i)

		longest_straight       = 0
		longest_straight_start = 0
		for i,piece in enumerate(self.pieces):
			if piece.angle == 0:
				to_turn = piece.length + piece.distance_to_turn
				if to_turn > longest_straight:
					longest_straight       = to_turn
					longest_straight_start = i
		self.pieces[longest_straight_start].turbo = True

		i = 0;
		loops = 3
		distance_to_switch = 0
		next_switch        = -1
		curr_piece = self.get_piece(i)
		prev_piece = self.get_prev_piece(i)
		while True:
			if curr_piece.index == 0:
				loops -= 1

			if curr_piece.switch:
				distance_to_switch = 0
				next_switch = curr_piece.index
			else:
				distance_to_switch += curr_piece.length

			if loops == 0 or (prev_piece.distance_to_switch,prev_piece.next_switch) == (distance_to_switch,next_switch):
				break
			else:
				prev_piece.distance_to_switch = distance_to_switch
				prev_piece.next_switch        = next_switch
				i -= 1
				curr_piece = prev_piece
				prev_piece = self.get_prev_piece(i)

	def get_piece(self, index):
		i = index % self.total_pieces
		return self.pieces[i]

	def get_next_piece(self, index):
		i = (index + 1) % self.total_pieces
		return self.pieces[i]

	def get_prev_piece(self, index):
		i = (index - 1) % self.total_pieces
		return self.pieces[i]

	def update_vmax(self,m):
		for p in self.pieces:
			p.update_vmax(m)

class Track(object):

	"""
		id
		name
		lanes
	"""

	def __init__(self, track):
		self.id     = track['id']
		self.name   = track['name']
		
		lanes = {}
		for lane in track['lanes']:
			index = int(lane['index'])
			l = Lane(lane)
			lanes[index] = l
		self.lanes = lanes

		pieces = track['pieces']
		for l in self.lanes.values():
			l.set_track_pieces(pieces)

	def get_lane_change(self, lane, index):
		index = self.lanes[lane].get_piece(index).next_switch
		if index == -1:
			return
		lane_left  = lane - 1
		lane_right = lane + 1
		length     = self.lanes[lane].get_piece(index).distance_to_switch
		change     = None
		if lane_left in self.lanes:
			l = self.lanes[lane_left].get_piece(index).distance_to_switch
			if l > length:
				change = 'Left'
		if lane_right in self.lanes:
			r = self.lanes[lane_right].get_piece(index).distance_to_switch
			if r > length:
				change = 'Right'
		return change

	def update_vmax(self,m):
		for l in self.lanes.values():
			l.update_vmax(m)

class Race(object):

	"""
		track		-- track
	"""

	def __init__(self, race):
		self.track = Track(race['track'])

class N(object):

	"""
		socket      -- client socket
		fsocket     -- socket as file
		name        -- bot name
		key         -- bot key

		debug       -- print debugging
		msg_no      -- keep track of messges when debugging
	"""

	# Protocol

	def __init__(self, client_socket, name, key):
		self.socket             = client_socket
		self.fsocket            = client_socket.makefile()
		self.name               = name
		self.key                = key
		self.msg_no             = 0
		self.debug              = 4 if socket.gethostname() in ('KN7','clifford') else 0
		self.prevpos            = 0
		self.prevpiece          = -1
		self.prevlap            = 0
		self.Z                  = 0
		self.F                  = 0
		self.M                  = 0.4
		self.marker             = (0,0)
		self.max_angle          = 0
		self.turbo_available    = False
		self.turbo_throttle_set = False
		self.switch_queued      = False
		self.switched           = False
		self.turbo              = False
		self.angle_data         = [
										( 1, 0.3  ),
										( 2, 0.25 ),
										( 5, 0.17 ),
										(10, 0.125),
										(20, 0.1  ),
										(30, 0.075),
										(40, 0.05 ),
										(47, 0.02 ),
										(50, 0.012),
										(52, 0.002),
								]
		self.trackM = {}

	def send(self, msg):
		if self.debug >= 10:
			print('{0} --> {1}'.format(self.msg_no, msg))
		self.socket.sendall(msg + "\n")

	def recv(self):
		line = self.fsocket.readline()
		if self.debug >= 10:
			self.msg_no += 1
			print('{0} <-- {1}'.format(self.msg_no, line))
		return line

	def msg(self, msg_type, data):
		self.send(json.dumps({"msgType": msg_type, "data": data}))

	def ping(self):
		self.msg("ping", {})

	"""
		race		-- current race
	"""

	# Race Control

	def join(self):
		self.msg(
			"join",
			{
				"name" : self.name,
				"key"  : self.key
			}
		)

	def create_race(self, track):
		self.msg(
			"createRace",
			{
				"botId" : {
							"name" : self.name,
							"key"  : self.key
						},
				"trackName" : track,
				"password"  : "Nilesh",
				"carcount"  : "1",
			}
		)

	def throttle(self, throttle):
		if throttle == 'TURBO':
			self.msg("turbo","turbo")
		else:
			self.msg("throttle", throttle)

	def switch_track(self, change):
		self.msg('switchLane',change)

	# Race State

	def on_create(self,msg):
		self.msg(
			"joinRace",
			{
				"botId" : {
							"name" : self.name,
							"key"  : self.key
						},
				"trackName" : "usa",
				"password"  : "Nilesh",
				"carcount"  : "1",
			}
		)

	def on_join(self, msg):
		if self.debug:
			print("Joined")
		self.ping()

	def on_game_init(self, msg):
		if self.debug:
			print("Game Init")
		data      = msg['data']
		self.race = Race(data['race'])
		self.race.track.update_vmax(self.M)
		if self.race.track.name in self.trackM:
			self.M = self.trackM[self.race.track.name]
			print('Using Cached M: %s' % self.M)
		self.ping()

	def on_your_car(self, msg):
		if self.debug:
			print("Car Assigned")
		data     = msg['data']
		self.car = data["color"]
		self.ping()

	def on_game_start(self, msg):
		if self.debug:
			print("Race Started")
		self.ping()

	def on_car_positions(self, msg):
		data     = msg['data']
		tick     = int(msg.get('gameTick',-1))
		velocity = 0
		if tick:
			for car in data:
				if car['id']['color'] == self.car:
					angle            = float(car['angle'])
					laneIndex        = int(car['piecePosition']['lane']['startLaneIndex'])
					lap              = int(car['piecePosition']['lap'])
					pieceIndex       = int(car['piecePosition']['pieceIndex'])
					inPieceDistance  = float(car['piecePosition']['inPieceDistance'])
					piece            = self.race.track.lanes[laneIndex].pieces[pieceIndex]
					pieceStart       = piece.start
					pieceRadius      = piece.radius
					pieceAngle       = piece.angle
					distance_to_turn = piece.length - inPieceDistance + piece.distance_to_turn
					next_turn        = piece.next_turn
					break
			else:
				self.throttle(0.1)
				return

			position       = inPieceDistance+pieceStart
			velocity       = position - self.prevpos
			self.prevpos   = position
			onNewPiece     = pieceIndex != self.prevpiece
			self.prevpiece = pieceIndex
			onNewLap       = lap != self.prevlap
			self.prevlap   = lap

			if tick < 1:
				self.initial_position = position
			else:
				travel = position - self.initial_position
				if self.F == 0 and travel > 0:
					self.F = travel
				elif self.F != 0 and self.Z == 0 and travel > 0:
					self.Z = (3*self.F - travel)/self.F

			mlap,mpiece = self.marker
			if lap > mlap and mpiece == pieceIndex:
				max_angle = self.max_angle
				M = self.M

				self.max_angle = 0
				self.marker = lap,pieceIndex

				self.trackM[self.race.track.name] = self.M

				for a in self.angle_data:
					if max_angle < a[0]:
						self.M += a[1]
						break

				print("M: %s -> %s, %s" % (M,self.M,max_angle))
				self.race.track.update_vmax(self.M)

			if self.max_angle < angle:
				self.max_angle = angle

			u_sq           = ' '
			a_dg           = ' '
			throttle       = 0.98765
			next_turn_v    = ' '
			next_turn_vmax = ' '
			curr_vmax      = ' '
			if self.Z > 0 and self.F > 0 and velocity > 0:
				u_sq = math.pow(velocity,2)
				a_dg = 2 * velocity * self.Z * distance_to_turn

				if velocity < 0:
					throttle = 0.01
				elif a_dg > u_sq:
					if piece.angle !=0 and piece.vmax < velocity:
						throttle = 0.0
					else:
						throttle = 1.0
				else:
					next_turn_vmax = self.race.track.lanes[laneIndex].pieces[piece.next_turn].vmax
					if velocity > next_turn_vmax:
						throttle = 0.0
					elif velocity < 0:
						throttle = 0.02
					else:
						throttle = 1.0
			else:
				throttle = 1.0
			
			change = None
			if onNewPiece and self.race.track.lanes[laneIndex].get_piece(pieceIndex - 1).switch:
				self.switch_queued = False
			if not self.switch_queued and self.Z and self.F and throttle != 'TURBO':
				change = self.race.track.get_lane_change(laneIndex, pieceIndex)

			if self.debug >= 5:
				if tick == 1:
					print('tick,lap,laneIndex,pieceIndex,F,Z,change,position,next_turn,distance_to_turn,velocity,u_sq,a_dg,next_turn_v,next_turn_vmax,curr_vmax,throttle,angle,pieceRadius,pieceAngle')
				print('%s,%s,%s,%s,%s,%s,%s,%s,%s,%s,%s,%s,%s,%s,%s,%s,%s,%s,%s,%s' % (tick,lap,laneIndex,pieceIndex,self.F,self.Z,change,position,next_turn,distance_to_turn,velocity,u_sq,a_dg,next_turn_v,next_turn_vmax,curr_vmax,throttle,angle,pieceRadius,pieceAngle))
			elif self.debug == 4:
				if tick and onNewPiece:
					print("%s %s" % (lap,pieceIndex))

			if change:
				self.switch_queued = True
				self.switch_track(change)
			else:
				self.throttle(throttle)

	def on_crash(self, msg):
		if self.debug:
			print("Crash")
		data = msg['data']
		if data['color'] == self.car:
			self.max_angle = 0
			self.M = self.M*0.7
			self.race.track.update_vmax(self.M)
			self.marker = (self.prevlap,(self.prevpiece + 2) % self.race.track.lanes[0].total_pieces)
			print("M>: %s" % self.M)
		self.ping()

	def on_game_end(self, msg):
		if self.debug:
			print("Race Ended")
		self.ping()

	def on_error(self, msg):
		if self.debug:
			print("Error: {0}".format(msg))
		self.ping()

	def on_turbo_available(self, msg):
		if self.debug:
			print("Turbo Available")
		data                 = msg['data']
		self.turbo_available = True
		self.turboFactor     = float(data['turboFactor'])
		self.turboDuration   = int(data.get('turboDurationTicks',0))

	def on_turbo_start(self, msg):
		data = msg['data']
		if data['color'] == self.car:
			self.turbo = True

	def on_turbo_end(self, msg):
		data = msg['data']
		if data['color'] == self.car:
			self.turbo = False

	# Run Bot

	def run(self):
		if self.debug:
			print('Run')
		#self.create_race('germany')
		self.join()
		self.msg_loop()

	def msg_loop(self):
		msg_map = {
			'createRace'     : self.on_create,
			'join'           : self.on_join,
			'yourCar'        : self.on_your_car,
			'gameInit'       : self.on_game_init,
			'gameStart'      : self.on_game_start,
			'carPositions'   : self.on_car_positions,
			'crash'          : self.on_crash,
			'gameEnd'        : self.on_game_end,
			'error'          : self.on_error,
			'turboAvailable' : self.on_turbo_available,
			'turboStart'     : self.on_turbo_start,
			'turboEnd'       : self.on_turbo_end,
		}
		line = self.recv()
		while line:
			msg = json.loads(line)
			msg_type = msg['msgType']
			if msg_type in msg_map:
				msg_map[msg_type](msg)
			else:
				if self.debug:
					print("Unknown -- {0}".format(msg_type))
				self.ping()
			sys.stdout.flush()
			line = self.recv()


if __name__ == "__main__":
	if len(sys.argv) != 5:
		print("Usage: ./run host port botname botkey")
	else:
		host, port, name, key = sys.argv[1:5]
		print("Connecting with parameters:")
		print("host={0}, port={1}, bot name={2}, key={3}".format(*sys.argv[1:5]))
		s = socket.socket(socket.AF_INET, socket.SOCK_STREAM)
		s.connect((host, int(port)))
		bot = N(s, name, key)
		bot.run()

